import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detalhes-serie',
  templateUrl: './detalhes-serie.page.html',
  styleUrls: ['./detalhes-serie.page.scss'],
})
export class DetalhesSeriePage{
  serieimg = "https://multiversonoticias.com.br/wp-content/uploads/2022/02/multiverso-2.jpg";
  serietitle = "CRIMINAL MINDS";
  serieinfo = "Este intenso procedimento policial segue um grupo de extraordinários perfis do FBI que passam seus dias entrando nas mentes de criminosos psicopatas.";
  seasons = "15 temporadas";
  parentalrating ="16";
  
  constructor() { }
}
