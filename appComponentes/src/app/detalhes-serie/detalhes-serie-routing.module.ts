import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalhesSeriePage } from './detalhes-serie.page';

const routes: Routes = [
  {
    path: '',
    component: DetalhesSeriePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalhesSeriePageRoutingModule {}
